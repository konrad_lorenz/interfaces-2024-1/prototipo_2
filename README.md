# Taller de Desarrollo: Aplicación Web de Búsqueda de Pokémon con Comandos de Voz

## Objetivo del Taller
El propósito de este taller es desarrollar una aplicación web que permita a los usuarios buscar información de Pokémon utilizando tanto entradas de texto como comandos de voz. Utilizando tecnologías web fundamentales como HTML, CSS y JavaScript, junto con bibliotecas adicionales como jQuery, Bootstrap y Artyom.js, los participantes aplicarán sus conocimientos en la construcción de interfaces de usuario, el manejo de eventos, la realización de solicitudes a APIs, y la integración de funcionalidades de voz para enriquecer la interacción del usuario.

## Requisitos del Prototipo

### Funcionalidades Básicas

- **Búsqueda por Nombre:** Los usuarios deben ser capaces de ingresar el nombre de un Pokémon para buscar su información a través de un campo de texto.
- **Visualización de Datos:** La aplicación debe mostrar la ficha técnica del Pokémon buscado, incluyendo su imagen, tipo, habilidades y cualquier otro dato relevante.
- **Comandos de Voz:** Implementar la capacidad de realizar búsquedas utilizando comandos de voz, gracias a la integración de Artyom.js.
- **Manejo de Errores:** Si se busca un Pokémon que no existe, la aplicación debe mostrar un mensaje de error 404 personalizado, indicando que el recurso no fue encontrado.
- **Botón de Inicio:** Incorporar un botón para iniciar la búsqueda de información de Pokémon.

### Requisitos Técnicos

- **HTML:** Estructurar el contenido de la aplicación, incluyendo el formulario de búsqueda, la sección de visualización de resultados y mensajes de error.
- **CSS & Bootstrap:** Utilizar CSS y Bootstrap para estilizar la aplicación, asegurando una interfaz atractiva y responsiva.
- **JavaScript & jQuery:** Desarrollar la lógica de la aplicación, manejo de eventos, solicitudes AJAX a la API de Pokémon y la integración de funcionalidades de comandos de voz con Artyom.js.

## Grupo Objetivo
Este taller está dirigido a estudiantes y aficionados de la programación web con conocimientos básicos en HTML, CSS y JavaScript, interesados en aprender cómo integrar APIs y funcionalidades de voz en sus proyectos. El objetivo de la aplicación es proporcionar una herramienta interactiva y educativa para explorar el mundo de Pokémon, promoviendo el aprendizaje sobre estas criaturas y el uso de tecnologías web modernas.

## Reporte
Se debe elaborar un reporte en LaTeX que detalle el proceso de desarrollo, incluyendo el Abstract, Introducción, Resultados y Conclusiones. Este documento debe ser conciso, con un máximo de 2 páginas, y debe subirse al repositorio junto con el código fuente.

## Entrega
El prototipo debe ser entregado a través de un repositorio en GitHub o GitLab, conteniendo todos los archivos necesarios para su ejecución. El README del repositorio debe ofrecer instrucciones claras sobre cómo ejecutar la aplicación y una descripción de las funcionalidades implementadas.

## Evaluación
La evaluación del prototipo se basará en los siguientes criterios:

- **Funcionalidad:** Verificación de las funcionalidades básicas y avanzadas requeridas.
- **Código:** Claridad, organización y eficiencia del código fuente.
- **Diseño:** Calidad visual de la interfaz de usuario y experiencia de usuario proporcionada.
- **Innovación:** Uso creativo de la tecnología y cómo se enriquece la experiencia del usuario con los comandos de voz.
